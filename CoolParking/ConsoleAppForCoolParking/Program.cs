﻿using System;

namespace ConsoleAppForCoolParking
{
    class Program
    {
        public static void Main()
        {
            var parkingFacade = new ParkingFacade();

            while (true)
            {
                Console.WriteLine("Choose command:");
                Console.WriteLine("1 - print parking balance");
                Console.WriteLine("2 - print current period income");
                Console.WriteLine("3 - print parking free places");
                Console.WriteLine("4 - print current period transactions");
                Console.WriteLine("5 - print transaction history");
                Console.WriteLine("6 - print vehicles list");
                Console.WriteLine("7 - add vehicle");
                Console.WriteLine("8 - remove vehicle");
                Console.WriteLine("9 - topup vehicle balance");
                Console.WriteLine("0 - exit");
                var commandString = Console.ReadLine();
                Console.WriteLine();
                try
                {
                    int command = int.Parse(commandString);

                    switch (command)
                    {
                        case 0:
                            return;
                        case 1:
                            parkingFacade.PrintParkingBalance();
                            break;
                        case 2:
                            parkingFacade.PrintCurrentPeriodIncome();
                            break;
                        case 3:
                            parkingFacade.PrintParkingFreePlaces();
                            break;
                        case 4:
                            parkingFacade.PrintCurrentPeriodTransactions();
                            break;
                        case 5:
                            parkingFacade.PrintTransactionHistory();
                            break;
                        case 6:
                            parkingFacade.PrintVehiclesList();
                            break;
                        case 7:
                            parkingFacade.AddVehicle();
                            break;
                        case 8:
                            parkingFacade.RemoveVehicle();
                            break;
                        case 9:
                            parkingFacade.TopUpVehicleBalance();
                            break;
                    }

                }
                catch (FormatException)
                {
                    Console.WriteLine($"Incorrect command");
                }
                Console.WriteLine();
            }

        }
    }
}
